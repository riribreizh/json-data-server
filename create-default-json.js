const fs = require('fs')

const files = [
  {
    name: 'config.json',
    content: {
      port: 4000,
      auth: {},
    },
  },
  {
    name: 'credentials.json',
    content: {
      passwords: []
    },
  },
  {
    name: 'db.json',
    content: {
      users: []
    }
  }
]

console.log('  checking needed files...')
files.forEach(file => {
  if (!fs.existsSync(file.name)) {
    console.log(`  - Creating ${file.name}`)
    fs.writeFileSync(file.name, JSON.stringify(file.content, null, 2))
  }
  else {
    console.log(`  - ${file.name} already exists!`)
  }
})
console.log('  done.')
