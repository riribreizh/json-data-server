import { readFileSync, writeFileSync } from 'fs'

export const readJson = file => JSON.parse(readFileSync(file))
export const writeJson = (file, data) => writeFileSync(file, JSON.stringify(data, null, 2))

export default {
  readJson,
  writeJson
}
