
export const HTTP = {
  OK: 200,
  BADREQUEST: 400,
  UNAUTHORIZED: 401,
  NOTFOUND: 404,
}
export default {
  HTTP,
}