import { Router } from 'express'
import { bodyParser } from 'json-server'
import credentials from './credentials'
import { HTTP } from './common'
import config from './config'

function identifiers (body) {
  return {
    id: body[config.reqUserId],
    pwd: body[config.reqUserPassword],
  }
}

function sendError (res, status, message) {
  res.status(status).jsonp(message).end()
}

// input validation (user id and password)
const userIdRE = new RegExp(config.userIdRE)
const userPwdRE = new RegExp(config.userPwd)

function validate ({ required }) { return (req, res, next) => {
  const { id, pwd } = identifiers(req.body)

  if (required && (!id || !id.trim() || !pwd || !pwd.trim())) {
    sendError(res, HTTP.BADREQUEST,
      `User identifier (${config.reqUserId}) and password (${config.reqUserPassword}) are required`)
  }
  else if (id && !userIdRE.test(id)) {
    sendError(res, HTTP.BADREQUEST,
      `Invalid user identifier format (checked against ${userIdRE.toString()})`)
  }
  else if (pwd && !userPwdRE.test(id)) {
    sendError(res, HTTP.BADREQUEST,
      `Invalid password format (checked against ${userPwdRE.toString()})`)
  }
  else {
    next()
  }
}}

function register(req, res/*, next*/) {
  const { db } = req.app
  const { id, pwd } = identifiers(req.body)
  delete req.body[config.reqUserPassword] // we store the user data without password

  const user = db.get(config.usersProp).find({ [config.userId]: id }).value()
  if (user) {
    sendError(res, HTTP.BADREQUEST, 'User with this identifier already exists')
  }
  else {
    credentials.setPassword(id, pwd).then(() => {
      credentials.setSession(id, req.ip).then(session => {
        db.get(config.usersProp).insert(req.body).write()
        res.status(HTTP.OK).jsonp(session)
      })
    })
  }
}

function login (req, res/*, next*/) {
  const { db } = req.app
  const { id, pwd } = identifiers(req.body)

  let user = db.get(config.usersProp).find({ [config.userId]: id }).value()
  if (!user) {
    sendError(res, HTTP.BADREQUEST, 'Incorrect user identifier or password')
  }
  else {
    credentials.checkPassword(id, pwd).then(same => {
      if (!same) {
        sendError(res, HTTP.BADREQUEST, 'Invalid password')
      }
      else {
        credentials.setSession(id, req.ip).then(session =>
          res.status(HTTP.OK).jsonp(session)
        )
    }
    }).catch(err => {
      sendError(res, HTTP.BADREQUEST, err)
    })
  }
}

function check (req, res/* next*/) {
  const token = req.body[config.jwtAccessToken]
  const { id } = identifiers(req.body)

  if (!id || !token) {
    sendError(res, HTTP.BADREQUEST,
      `User identifier (${config.reqUserId}) and refresh token (${config.jwtAccessToken}) are required`)
  }
  else {
    credentials.checkSession(id, token).then(() => {
      res.status(HTTP.OK).jsonp({ valid: true })
    }).catch(err => {
      res.status(HTTP.OK).jsonp({
        valid: false,
        reason: err.message
      })
    })
  }
}

function refresh (req, res/* next*/) {
  const { id } = identifiers(req.body)
  const token = req.body[config.jwtRefreshToken]

  if (!id || !token) {
    sendError(res, HTTP.BADREQUEST,
        `User identifier (${config.reqUserId} and refresh token (${config.jwtRefreshToken}) are required`)
  }
  else {
    credentials.checkSession(id, token).then(() => {
      credentials.setSession(id, req.ip).then(session => {
        res.status(HTTP.OK).jsonp(session)
      })
    })
  }
}

function password (req, res/*, next*/) {
  const { db } = req.app
  const { id, pwd } = identifiers(req.body)
  const newpwd = req.body[config.reqNewPassword]

  if (!newpwd) {
    sendError(res, HTTP.BADREQUEST, `New password (${config.reqNewPassword}) cannot be empty`)
    return
  }
  else {
    const user = db.get(config.usersProp).find({ [config.userId]: id }).value()
    if (!user) {
      res.status(HTTP.BADREQUEST).jsonp('User does not exist')
    }
    else {
      Promise.all([
        credentials.checkPassword(id, pwd),
        credentials.setPassword(id, newpwd),
        credentials.setSession(id, req.ip)
      ]).then(all => {
        if (!all[0]) {
          res.status(HTTP.BADREQUEST).jsonp('Invalid current password')
        }
        else {
          res.status(HTTP.OK).jsonp(all[2])
        }
      })
    }
  }
}

function checkBearer (req, res, next) {
  const token = req.headers.authorization ? req.headers.authorization.replace(/^Bearer /, '') : null
  if (!token) {
    sendError(res, HTTP.UNAUTHORIZED, 'No authorization')
  }
  else {
    next()
  }
}

const router = Router().use(bodyParser)
const routes = {
  register: {
    path: config.registerRoute,
    validation: true,
    handler: register,
  },
  login: {
    path: config.loginRoute,
    validation: true,
    handler: login,
  },
  logout: {
    path: config.logoutRoute,
    validation: true,
    handler: null,
  },
  check: {
    path: config.checkRoute,
    validation: false,
    handler: check,
  },
  refresh: {
    path: config.refreshRoute,
    validation: false,
    handler: refresh,
  },
  password: {
    path: config.passwordRoute,
    validation: true,
    handler: password,
  },
}

console.log('Authentication module:')
Object.keys(routes).forEach(name => {
  const route = routes[name]
  if (!route.handler) {
    console.log(`  ${name} unimplemented`)
  }
  else if (route.path) {
    console.log(`  ${name} => ${route.path}`)
    router.post(route.path, validate({ required: route.validation }), route.handler)
  }
  else {
    console.log(`  ${name} disabled by config`)
  }
})
router.use(checkBearer)
export default router
