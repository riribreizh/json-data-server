// manage credentials database (default credentials.json)

import bcrypt from 'bcryptjs'
import jsonwebtoken from 'jsonwebtoken'
import config from './config'
import { readJson, writeJson } from './utils'

// TODO: make it watchable to reload in case of a manual change
const credentials = readJson(config.credentialsFile)
if (!credentials.passwords) {
  credentials.passwords = []
  writeJson(config.credentialsFile, credentials)
}

// find item in credentials' passwords and sessions
const getIdentity = sub => credentials.passwords.find(item => item.sub === sub)
//const getSession = (sub, ip) => credentials.sessions.find(item => item.sub === sub && item.ip === ip)

// make jwt Promise'd
const jwt = {
  sign: (claims, secret, options) => new Promise((resolve, reject) => {
    jsonwebtoken.sign(claims, secret, options, (err, token) => {
      err ? reject(err) : resolve(token)
    })
  }),
  verify: (token, secret, options) => new Promise((resolve, reject) => {
    jsonwebtoken.verify(token, secret, options, (err, claims) => {
      err ? reject(err) : resolve(claims)
    })
  }),
  decode: (token, options) => new Promise((resolve, reject) => {
    jsonwebtoken.decode(token, options, (err, claims) => {
      err ? reject(err) : resolve(claims)
    })
  }),
}

export default {

  setPassword: (sub, password) => {
    const identity = getIdentity(sub)
    password = bcrypt.hashSync(password, config.passwordSalt)
    if (identity) {
      identity.password = password
    }
    else {
      credentials.passwords.push({ sub, password })
    }
    writeJson(config.credentialsFile, credentials)
    return Promise.resolve(true)
  },

  checkPassword: async (sub, password) => {
    return new Promise((resolve, reject) => {
      const identity = getIdentity(sub)
      if (!identity) {
        reject('Identity not found')
      }
      bcrypt.compare(password, identity.password).then(same => {
        resolve(same)
      })
    })
  },

  setSession: async (sub, ip) => {
    // create access and refresh token
    const accessToken = await jwt.sign({ ip }, config.jwtSecret, {
      expiresIn: config.jwtExpiresIn,
      subject: String(sub)
    })
    const refreshToken = await jwt.sign({}, config.jwtSecret, {
      expiresIn: config.jwtRefreshExpiresIn,
      subject: String(sub)
    })
    return Promise.resolve({
      [config.jwtAccessToken]: accessToken,
      [config.jwtRefreshToken]: refreshToken
    })
  },

  checkSession: async (sub, token) => {
    return jwt.verify(token, config.jwtSecret, {
      subject: String(sub),
      clockTolerance: config.jwtClockTolerance,
    })
  },

}
