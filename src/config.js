import { readJson } from'./utils'

// get json-server config file from arguments, or default value
// default: https://github.com/typicode/json-server/blob/master/src/cli/index.js
const i = process.argv.findIndex(arg => /-c|--config/.test(arg))
const config = readJson((i > -1) ? process.argv[i + 1] : './json-server.json').auth

// set of default values when not in loaded config
const defaults = {
  registerRoute: '/auth/register',
  loginRoute: '/auth/login',
  logoutRoute: '/auth/logout',
  checkRoute: '/auth/check',
  refreshRoute: '/auth/refresh',
  passwordRoute: '/auth/password',
  credentialsFile: 'credentials.json',
  usersProp: 'users',
  userId: 'id',
  reqUserId: 'username',
  reqUserPassword: 'password',
  reqNewPassword: 'newpassword',
  userIdRE: '.*',
  userPwdRE: '.*',
  passwordSalt: 10,
  jwtSecret: 'DefaultJWTSecretString-LOL',
  jwtAccessToken: 'access_token',
  jwtRefreshToken: 'refresh_token',
  jwtExpiresIn: '1m',
  jwtRefreshExpiresIn: '1d',
  jwtClockTolerance: 2,
}

// export merge of defaults and loaded config
const mergedConfig = Object.assign({}, defaults, config)
export default mergedConfig
