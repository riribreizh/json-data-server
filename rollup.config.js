import json from 'rollup-plugin-json'
import pkg from './package.json'

export default {
  input: pkg.source,
  output: [
    { file: pkg.main, format: 'cjs' },
    { file: pkg.module, format: 'es' },
  ],
  //external: [ 'bcryptjs', 'express', 'fs', 'json-server', 'jsonwebtoken' ],
  external: [ 'fs', ...Object.keys(pkg.dependencies) ],
  plugins: [ json(), ]
}
