'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var express = require('express');
var jsonServer = require('json-server');
var bcrypt = _interopDefault(require('bcryptjs'));
var jsonwebtoken = _interopDefault(require('jsonwebtoken'));
var fs = require('fs');

const readJson = file => JSON.parse(fs.readFileSync(file));
const writeJson = (file, data) => fs.writeFileSync(file, JSON.stringify(data, null, 2));

// get json-server config file from arguments, or default value
// default: https://github.com/typicode/json-server/blob/master/src/cli/index.js
const i = process.argv.findIndex(arg => /-c|--config/.test(arg));
const config = readJson((i > -1) ? process.argv[i + 1] : './json-server.json').auth;

// set of default values when not in loaded config
const defaults = {
  registerRoute: '/auth/register',
  loginRoute: '/auth/login',
  logoutRoute: '/auth/logout',
  checkRoute: '/auth/check',
  refreshRoute: '/auth/refresh',
  passwordRoute: '/auth/password',
  credentialsFile: 'credentials.json',
  usersProp: 'users',
  userId: 'id',
  reqUserId: 'username',
  reqUserPassword: 'password',
  reqNewPassword: 'newpassword',
  userIdRE: '.*',
  userPwdRE: '.*',
  passwordSalt: 10,
  jwtSecret: 'DefaultJWTSecretString-LOL',
  jwtAccessToken: 'access_token',
  jwtRefreshToken: 'refresh_token',
  jwtExpiresIn: '1m',
  jwtRefreshExpiresIn: '1d',
  jwtClockTolerance: 2,
};

// export merge of defaults and loaded config
const mergedConfig = Object.assign({}, defaults, config);

// manage credentials database (default credentials.json)

// TODO: make it watchable to reload in case of a manual change
const credentials = readJson(mergedConfig.credentialsFile);
if (!credentials.passwords) {
  credentials.passwords = [];
  writeJson(mergedConfig.credentialsFile, credentials);
}

// find item in credentials' passwords and sessions
const getIdentity = sub => credentials.passwords.find(item => item.sub === sub);
//const getSession = (sub, ip) => credentials.sessions.find(item => item.sub === sub && item.ip === ip)

// make jwt Promise'd
const jwt = {
  sign: (claims, secret, options) => new Promise((resolve, reject) => {
    jsonwebtoken.sign(claims, secret, options, (err, token) => {
      err ? reject(err) : resolve(token);
    });
  }),
  verify: (token, secret, options) => new Promise((resolve, reject) => {
    jsonwebtoken.verify(token, secret, options, (err, claims) => {
      err ? reject(err) : resolve(claims);
    });
  }),
  decode: (token, options) => new Promise((resolve, reject) => {
    jsonwebtoken.decode(token, options, (err, claims) => {
      err ? reject(err) : resolve(claims);
    });
  }),
};

var credentials$1 = {

  setPassword: (sub, password) => {
    const identity = getIdentity(sub);
    password = bcrypt.hashSync(password, mergedConfig.passwordSalt);
    if (identity) {
      identity.password = password;
    }
    else {
      credentials.passwords.push({ sub, password });
    }
    writeJson(mergedConfig.credentialsFile, credentials);
    return Promise.resolve(true)
  },

  checkPassword: async (sub, password) => {
    return new Promise((resolve, reject) => {
      const identity = getIdentity(sub);
      if (!identity) {
        reject('Identity not found');
      }
      bcrypt.compare(password, identity.password).then(same => {
        resolve(same);
      });
    })
  },

  setSession: async (sub, ip) => {
    // create access and refresh token
    const accessToken = await jwt.sign({ ip }, mergedConfig.jwtSecret, {
      expiresIn: mergedConfig.jwtExpiresIn,
      subject: String(sub)
    });
    const refreshToken = await jwt.sign({}, mergedConfig.jwtSecret, {
      expiresIn: mergedConfig.jwtRefreshExpiresIn,
      subject: String(sub)
    });
    return Promise.resolve({
      [mergedConfig.jwtAccessToken]: accessToken,
      [mergedConfig.jwtRefreshToken]: refreshToken
    })
  },

  checkSession: async (sub, token) => {
    return jwt.verify(token, mergedConfig.jwtSecret, {
      subject: String(sub),
      clockTolerance: mergedConfig.jwtClockTolerance,
    })
  },

};

const HTTP = {
  OK: 200,
  BADREQUEST: 400,
  UNAUTHORIZED: 401,
  NOTFOUND: 404,
};

function identifiers (body) {
  return {
    id: body[mergedConfig.reqUserId],
    pwd: body[mergedConfig.reqUserPassword],
  }
}

function sendError (res, status, message) {
  res.status(status).jsonp(message).end();
}

// input validation (user id and password)
const userIdRE = new RegExp(mergedConfig.userIdRE);
const userPwdRE = new RegExp(mergedConfig.userPwd);

function validate ({ required }) { return (req, res, next) => {
  const { id, pwd } = identifiers(req.body);

  if (required && (!id || !id.trim() || !pwd || !pwd.trim())) {
    sendError(res, HTTP.BADREQUEST,
      `User identifier (${mergedConfig.reqUserId}) and password (${mergedConfig.reqUserPassword}) are required`);
  }
  else if (id && !userIdRE.test(id)) {
    sendError(res, HTTP.BADREQUEST,
      `Invalid user identifier format (checked against ${userIdRE.toString()})`);
  }
  else if (pwd && !userPwdRE.test(id)) {
    sendError(res, HTTP.BADREQUEST,
      `Invalid password format (checked against ${userPwdRE.toString()})`);
  }
  else {
    next();
  }
}}

function register(req, res/*, next*/) {
  const { db } = req.app;
  const { id, pwd } = identifiers(req.body);
  delete req.body[mergedConfig.reqUserPassword]; // we store the user data without password

  const user = db.get(mergedConfig.usersProp).find({ [mergedConfig.userId]: id }).value();
  if (user) {
    sendError(res, HTTP.BADREQUEST, 'User with this identifier already exists');
  }
  else {
    credentials$1.setPassword(id, pwd).then(() => {
      credentials$1.setSession(id, req.ip).then(session => {
        db.get(mergedConfig.usersProp).insert(req.body).write();
        res.status(HTTP.OK).jsonp(session);
      });
    });
  }
}

function login (req, res/*, next*/) {
  const { db } = req.app;
  const { id, pwd } = identifiers(req.body);

  let user = db.get(mergedConfig.usersProp).find({ [mergedConfig.userId]: id }).value();
  if (!user) {
    sendError(res, HTTP.BADREQUEST, 'Incorrect user identifier or password');
  }
  else {
    credentials$1.checkPassword(id, pwd).then(same => {
      if (!same) {
        sendError(res, HTTP.BADREQUEST, 'Invalid password');
      }
      else {
        credentials$1.setSession(id, req.ip).then(session =>
          res.status(HTTP.OK).jsonp(session)
        );
    }
    }).catch(err => {
      sendError(res, HTTP.BADREQUEST, err);
    });
  }
}

function check (req, res/* next*/) {
  const token = req.body[mergedConfig.jwtAccessToken];
  const { id } = identifiers(req.body);

  if (!id || !token) {
    sendError(res, HTTP.BADREQUEST,
      `User identifier (${mergedConfig.reqUserId}) and refresh token (${mergedConfig.jwtAccessToken}) are required`);
  }
  else {
    credentials$1.checkSession(id, token).then(() => {
      res.status(HTTP.OK).jsonp({ valid: true });
    }).catch(err => {
      res.status(HTTP.OK).jsonp({
        valid: false,
        reason: err.message
      });
    });
  }
}

function refresh (req, res/* next*/) {
  const { id } = identifiers(req.body);
  const token = req.body[mergedConfig.jwtRefreshToken];

  if (!id || !token) {
    sendError(res, HTTP.BADREQUEST,
        `User identifier (${mergedConfig.reqUserId} and refresh token (${mergedConfig.jwtRefreshToken}) are required`);
  }
  else {
    credentials$1.checkSession(id, token).then(() => {
      credentials$1.setSession(id, req.ip).then(session => {
        res.status(HTTP.OK).jsonp(session);
      });
    });
  }
}

function password (req, res/*, next*/) {
  const { db } = req.app;
  const { id, pwd } = identifiers(req.body);
  const newpwd = req.body[mergedConfig.reqNewPassword];

  if (!newpwd) {
    sendError(res, HTTP.BADREQUEST, `New password (${mergedConfig.reqNewPassword}) cannot be empty`);
    return
  }
  else {
    const user = db.get(mergedConfig.usersProp).find({ [mergedConfig.userId]: id }).value();
    if (!user) {
      res.status(HTTP.BADREQUEST).jsonp('User does not exist');
    }
    else {
      Promise.all([
        credentials$1.checkPassword(id, pwd),
        credentials$1.setPassword(id, newpwd),
        credentials$1.setSession(id, req.ip)
      ]).then(all => {
        if (!all[0]) {
          res.status(HTTP.BADREQUEST).jsonp('Invalid current password');
        }
        else {
          res.status(HTTP.OK).jsonp(all[2]);
        }
      });
    }
  }
}

function checkBearer (req, res, next) {
  const token = req.headers.authorization ? req.headers.authorization.replace(/^Bearer /, '') : null;
  if (!token) {
    sendError(res, HTTP.UNAUTHORIZED, 'No authorization');
  }
  else {
    next();
  }
}

const router = express.Router().use(jsonServer.bodyParser);
const routes = {
  register: {
    path: mergedConfig.registerRoute,
    validation: true,
    handler: register,
  },
  login: {
    path: mergedConfig.loginRoute,
    validation: true,
    handler: login,
  },
  logout: {
    path: mergedConfig.logoutRoute,
    validation: true,
    handler: null,
  },
  check: {
    path: mergedConfig.checkRoute,
    validation: false,
    handler: check,
  },
  refresh: {
    path: mergedConfig.refreshRoute,
    validation: false,
    handler: refresh,
  },
  password: {
    path: mergedConfig.passwordRoute,
    validation: true,
    handler: password,
  },
};

console.log('Authentication module:');
Object.keys(routes).forEach(name => {
  const route = routes[name];
  if (!route.handler) {
    console.log(`  ${name} unimplemented`);
  }
  else if (route.path) {
    console.log(`  ${name} => ${route.path}`);
    router.post(route.path, validate({ required: route.validation }), route.handler);
  }
  else {
    console.log(`  ${name} disabled by config`);
  }
});
router.use(checkBearer);

module.exports = router;
